﻿using crud_operations.data.Models;
using crud_operations_webapi.Infrastructure.IRepository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace crud_operations_webapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeAsyncOpsController : ControllerBase
    {
        private IEmployeeRepositoryAsyncOps _context;

        public EmployeeAsyncOpsController(IEmployeeRepositoryAsyncOps context)
        {
            _context = context;
        }

        [HttpGet]
        public Task<IEnumerable<Employee>> GetEmployees()
        {

        }
    }
}
