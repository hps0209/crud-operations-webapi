﻿using Azure;
using crud_operations.data.Models;
using crud_operations_webapi.Infrastructure.IRepository;
using Microsoft.AspNetCore.Mvc;

namespace crud_operations_webapi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private IEmployeeRepository _employeeRepository;

        public EmployeeController(IEmployeeRepository employeeRepository)
        {
            this._employeeRepository = employeeRepository;
        }

        [HttpGet]
        public IEnumerable<Employee> GetEmployees()
        {
            return _employeeRepository.GetEmployees();
        }

        [HttpGet("{empId}")]
        public Employee GetEmployees([FromRoute]int empId)
        {
            return _employeeRepository.GetEmployee(empId);
        }

        [HttpPost]
        public IActionResult Post([FromBody] Employee employee)
        {
            var result = _employeeRepository.SaveEmployee(employee);

            if (result == true)
            {
                return Ok(result);
            }
            return BadRequest(result);
        }

        [HttpDelete("{empId}")] 
        public IActionResult DeleteEmployees([FromRoute]int empId)
        {
            if(empId<1)
            {
                return BadRequest();
            }

            var result= _employeeRepository.DeleteEmployee(empId);

            if (result == true)
            {
                return Ok();
            }
            return BadRequest();
        }

        [HttpPut("{empid}")]
        public IActionResult Put([FromBody]Employee employee, [FromRoute] int empid)
        {
            var result=_employeeRepository.PutEmployee(employee,empid);
            if (result==true)
            {
                return Ok(result);
            }
            return BadRequest();
        }

        //[HttpPatch("{empid}")]
        //public IActionResult Post([FromBody] JsonPatchDocument employee, [FromRoute] int empid)
        //{
        //    var result = _employeeRepository.PutEmployee(employee, empid);
        //    if (result == true)
        //    {
        //        return Ok(result);
        //    }
        //    return BadRequest();
        //}

     
    }
}
