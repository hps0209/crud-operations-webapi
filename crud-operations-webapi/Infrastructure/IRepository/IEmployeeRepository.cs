﻿using Azure;
using crud_operations.data.Models;

namespace crud_operations_webapi.Infrastructure.IRepository
{
    public interface IEmployeeRepository
    {
        IEnumerable<Employee> GetEmployees();
        Employee GetEmployee(int empId);
        bool SaveEmployee(Employee employee);
        bool DeleteEmployee(int empId);
        bool PutEmployee(Employee employee,int empId);
        //bool PatchEmployee(JsonPatchDocument employee,int empId);
    }
}
