﻿using crud_operations.data.Models;

namespace crud_operations_webapi.Infrastructure.IRepository
{
    public class IEmployeeRepositoryAsyncOps
    {
        async Task<IEnumerable<Employee>> GetEmployeesAsync();
    }
}
