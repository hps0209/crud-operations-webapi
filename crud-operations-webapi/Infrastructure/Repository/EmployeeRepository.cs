﻿using Azure;
using crud_operations.data.Models;
using crud_operations_webapi.Infrastructure.IRepository;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.EntityFrameworkCore;

namespace crud_operations_webapi.Infrastructure.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly PracticeContext _context;

        public EmployeeRepository(PracticeContext context)
        {
            _context = context;
        }

        public bool DeleteEmployee(int empId)
        {
            var emp=_context.Employees.Find(empId);

            if(emp != null)
            {
                _context.Employees.Remove(emp);
            }
            else
            {
                return false;
            }

            _context.SaveChanges();
            return true;
        }

        public IEnumerable<Employee> GetEmployees()
        {
            return _context.Employees.ToList();
        }

        public Employee GetEmployee(int empId)
        {
            var emp= _context.Employees.Find(empId);
            //if (emp != null)
            //{
            //    return emp;
            //}
            ////return null;
            return emp;
        }

        public bool PutEmployee(Employee employee, int empId)
        {
            var emp = new Employee()
            {
                Id = empId,
                Name = employee.Name,
                Department = employee.Department,
                Email = employee.Email,
                Mobile = employee.Mobile,
                Salary = employee.Salary
            };
            _context.Employees.Update(emp);

            if (_context.SaveChanges() == 1)
                return true;
            else return false;
        }

        //public bool PatchEmployee(JsonPatchDocument employee,int empId)
        //{
        //    var emp = _context.Employees.Find(empId);
        //    if(emp != null )
        //    {
        //        employee
        //    }
        //}

        public bool SaveEmployee(Employee employee)
        {
            var emp = new Employee
            {
                Name = employee.Name,
                Department = employee.Department,
                Email = employee.Email,
                Mobile = employee.Mobile,
                Salary = employee.Salary
            };

            _context.Employees.Add(emp);
            if(_context.SaveChanges()==1)
                return true;
            else return false;
        }
    }
}
