﻿using System;
using System.Collections.Generic;

namespace crud_operations.data.Models;

public partial class Department
{
    public int DeptId { get; set; }

    public string? DeptName { get; set; }
}
