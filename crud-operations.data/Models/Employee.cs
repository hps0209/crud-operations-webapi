﻿using System;
using System.Collections.Generic;

namespace crud_operations.data.Models;

public partial class Employee
{
    public int Id { get; set; }

    public string? Name { get; set; }

    public string? Department { get; set; }

    public string? Email { get; set; }

    public string? Mobile { get; set; }

    public long? Salary { get; set; }
}
